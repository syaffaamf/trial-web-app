# Monitoring system with Grafana & Prometheus
![](img/tampilan-awal-grafana.png)
![](img/tampilan-menuawal-grafana.png)
![](img/tampilan-dashboard-grafana.png)

## Description system

This is a server monitoring system based on Docker and Grafana which is able to provide user interfaces and a user experience that is easier to understand.

This monitoring system can provide information in real time and is able to provide alerts when certain conditions are reached

## Instalation

This is step for installation proses of the system


1. Docker Instalation
    1. Update existing list of package
        ```
        sudo apt-get update
        ```
    2. Set up Docker's apt repository
        - Add Docker's official GPG key:
            ```sudo apt-get install ca-certificates curl gnupg
            sudo install -m 0755 -d /etc/apt/keyrings
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
            sudo chmod a+r /etc/apt/keyrings/docker.gpg```
        - Add the repository to Apt sources:
            - `echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \`
  
            ```
            sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
            sudo apt-get update
            ```
  
    3. Install the Docker Packages
        ```
        sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
        ```

    4. Verify that the Docker Engine installation is successful
        ```
        sudo service docker start 
        sudo sysctemctl status docker
        ```
    5. Docker Compose installation
        ``` 
        sudo curl -L "https://github.com/docker/compose/releases/download/$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep '\"tag_name\":' | sed -E 's/.*\"([^\"]+)\".*/\1/')/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
        docker-compose --version
        ```

2. Initiation **Grafana**, **Prometheus**, **Node eksporter** with  **Docker Compose**

    1. Initiation Docker Compose 
        ```
        docker-compose up -d
        ```
    2. Check file Prometheus configuration
        ```
        cd /trial-web-app/monitoring/config/prometheus/
        nano prometheus.yml
        ```
        Check Configuration **prometheus.yml**
        ```
        global:
        scrape_interval: 10s
        evaluation_interval: 10s
            scrape_configs:
            -   job_name: "prometheus"
                scrape_interval: 5s
                static_configs:
                -   targets: ["103.84.207.141:9090"]
            -  job_name: "node"
                scrape_interval: 5s
                static_configs:
                -   targets: ["103.84.207.141:9100"]
            -   job_name: "node-2"
                scrape_interval: 5s
                static_configs:
                -   targets: ["167.71.208.182:9100"]
            -   job_name: "Docker-Container"
                scrape_interval: 5s
                static_configs:
                -   targets: ["103.84.207.141:8080"]
        ```
        <br/>Note : **adjust the target IP to the destination**

    3. Check Prometheus Configuration in **Docker Compose**
        ```
        cd /trial-web-app/monitoring/
        nano docker-compose.yml
        ```
        Check Configuration prometheus in **docker-compose.yml**
        ```
        version: '3'
        volumes:
        prometheus_data:
        name: prometheus_data
        driver: local

        grafana_data:
            name: grafana_data
            driver: local

        networks:
            monitoring:
            external: true

        services:
            prometheus:
                container_name: prometheus
                image: prom/prometheus
                restart: unless-stopped
                volumes:
                    - '/trial-web-app/monitoring/config/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml'
                    - 'prometheus_data:/prometheus'
                command:
                    - '--config.file=/etc/prometheus/prometheus.yml'
                    - '--storage.tsdb.path=/prometheus'
                    - '--web.console.libraries=/usr/share/prometheus/console_libraries'
                    - '--web.console.templates=/usr/share/prometheus/consoles'
                networks:
                    - monitoring
                ports:
                    - 9090:9090
            
            node-exporter:
                #    network_mode: host
                pid: host
                restart: unless-stopped
                volumes:
                    - '/:/host:ro,rslave'
                image: 'quay.io/prometheus/node-exporter:latest'
                networks:
                    - monitoring
                ports:
                    - 9100:9100
            
            cadvisor:
                image: google/cadvisor:latest
                pid: host
                restart: unless-stopped
                volumes:
                    - '/:/host:ro,rslave'
                ports:
                    - 8080:8080
                networks:
                    - monitoring

            #  snmp_exporter:
            #    container_name: snmp_exporter
            #    image: prom/snmp-exporter
            #    restart: unless-stopped
            #    networks:
            #      - monitoring

            grafana:
                container_name: grafana
                image: grafana/grafana
                restart: unless-stopped
                volumes:
                    - 'grafana_data:/var/lib/grafana'
                    - './config/grafana:/etc/grafana/provisioning/datasources'
                    - '/etc/localtime:/etc/localtime:ro'
                networks:
                    - monitoring
                ports:
                    - 5000:3000

        ```

        <br/>Note: **adjust the volume location in the prometheus section according to the location of prometheus.yml**

     4. **Re-Initiation** Docker Compose & **restart** prometheus container 

        ```
        sudo docker-compose up -d 
        docker restart prometheus
        ```

## How to Access Grafana Dashboard

This is step for accessing **Grafana Dashboard**

![](img/tampilan-awal-grafana.png)

1. Enter the access link (http://youripaddress:5000)
    
    http://monitoring-1.kipxmsib5.cloud/

    or

    http://103.84.207.141:5000/

2. Log in with admin account
    ```
    username = admin
    password = admin
    ```
3. Choose the **Node Eksporter Full** Dashboard
![](img/tampilan-pilihan-dashboard.png)

## Setting Contact Point for Alerting in Grafana Dashboard

This is step for setting **contact point** for alerting rule in  Grafana Dashboard


1. Use SMTP Email

    1. Clone & edit file **grafana.ini**
        ```
        cd /home/test/test/
        docker cp grafana:/etc/grafana/grafana.ini /home/test/test/
        nano grafana.ini
        ```
    2. Edit & change configuration file **grafana.ini**
        - Use **CRTL + W** on your keyboard for search word **"SMTP"**
        ![](img/tampilan-before-smtp.png)
        ![](img/tampilan-after-smtp.png)
        <br/>Note : Don't Forget to activate your GMAIL SMTP

    4. Go to Grafana Dashboard and **Enter Contact Points** in Side-Navigation and **Change Adresses**
    ![](img/tampilan-contactpoints.png)



2. Use Telegram Bot

    1. Open Your Telegram and Search **BotFather**
        ![](img/tampilan-telegram-botfather.png)
        <br/>Note : type "/start" in the botfather chat column
    
    2. Create New Bot
        ![](img/tampilan-telegram-createnewbot.png)
        <br/>Note : After your type "/newbot", you can give name for your bot

    3. Acces Your New Bot & Get the Access Token
        ![](img/tampilan-telegram-gettoken.png)
        <br/>Note : You will get a link for your new bot and an **access token** to use in Grafana
    
    4. Open Your Telegram and Search **Get ID bot**
        ![](img/tampilan-telegram-getidbot.png)
        <br/>Note : You will get a your **Chat ID** to use in Grafana
    
    5. Input the **Access Token** and **Chat ID** in Grafana Dashboard
        ![](img/tampilan-grafana-enterbottele.png)
        <br/>Note : You can Test after your input 

        ![](img/tampilan-grafana-test.png)
        <br/>Note : If you get this notification, you will **success** to connect
         your bot and your 
        grafana




















