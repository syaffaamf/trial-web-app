import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// Tambahkan pengaturan server untuk mendengarkan alamat IP publik
export default defineConfig({
  plugins: [react()],
  server: {
    host: '0.0.0.0', // Mendengarkan alamat IP eksternal
    port: 3000, // Gantilah dengan port yang sesuai
  },
});

